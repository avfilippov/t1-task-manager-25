package ru.t1.avfilippov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.command.AbstractCommand;
import ru.t1.avfilippov.tm.exception.entity.UserNotFoundException;
import ru.t1.avfilippov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
