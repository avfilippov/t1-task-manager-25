package ru.t1.avfilippov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Status;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "complete project by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
    }

}
