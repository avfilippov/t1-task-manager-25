package ru.t1.avfilippov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.model.User;

public interface IAuthService {

    void checkRoles(@Nullable Role[] roles);

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    boolean isAuth();

    @Nullable
    String getUserId();

    @NotNull
    User getUser();

}
